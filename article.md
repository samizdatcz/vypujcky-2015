---
title: "Osm milionů výpůjček: Co Češi čtou?"
perex: "Pokud necháváte nákup dárků na poslední chvíli a nakonec stejně koupíte knížku, můžeme vám poradit, komu a jakou."
description: "Pokud necháváte nákup dárků na poslední chvíli a nakonec stejně koupíte knížku, můžeme vám poradit, komu a jakou."
authors: ["Jan Boček"]
published: "23. prosince 2016"
coverimg: https://interaktivni.rozhlas.cz/data/vypujcky-2015/www/media/cover.jpg
coverimg_note: "Foto <a href='https://www.flickr.com/photos/suchosch/6501732911/'>České knihy ve stutgartské knihovně | Suchosch</a>"
socialimg: https://interaktivni.rozhlas.cz/data/vypujcky-2015/www/media/socialimg.jpg
url: "vypujcky-2015"
libraries: [jquery, highcharts]
recommended:
  - link: https://interaktivni.rozhlas.cz/startovace-text/
    title: Projekty na Hithitu a Startovači získaly prvních 100 milionů korun
    perex: Podívejte se, kdo uspěl.
    image: https://interaktivni.rozhlas.cz/startovace-text/media/cover.jpg
  - link: https://interaktivni.rozhlas.cz/cestina/
    title: Data o znalostech češtiny: Nejtěžší jsou velká písmena
    perex: Vyzkoušejte si, co dělá problémy vám.
    image: https://interaktivni.rozhlas.cz/cestina/media/decko.jpg
  - link: http://www.rozhlas.cz/zpravy/data/_zprava/jizni-mesto-sidliste-bez-lidi--1532519
    title: Jižní Město – sídliště bez lidí?
    perex: Skoro deset tisíc lidí zmizelo z největšího tuzemského sídliště od roku 2001. Kam a proč?
    image: https://samizdat.blob.core.windows.net/storage/jizni-mesto-2.jpg
---

Pražská městská knihovna loni zapůjčila osm milionů knížek, časopisů nebo filmů. Anonymizovaná data ukazují, jaké mají Češi čtenářské návyky: kdo, co a kdy čte.

První pohled na data prozrazuje, že české ženy čtou – nebo si aspoň půjčují knihy – třikrát víc než muži. V některých věkových skupinách je rozdíl zvlášť patrný: například po dvacátém roce života se prudce sníží počet čtoucích mužů, po čtyřicátém roce zase nejvíc propadnou knihám ženy.

<aside class="big">
  <iframe src="https://interaktivni.rozhlas.cz/data/vypujcky-2015/charts/skupiny.htm" width="100%" height="400" scrolling="no" frameborder="0"></iframe>
</aside>

Seznam vůbec nejpůjčovanějších knih kupodivu nekopíruje aktuální čtenářské trendy, mezi dvaceti vůbec nejpůjčovanějšími například není žádná severská detektivka. Zato svazků povinné četby je mezi nimi hned několik.

<aside class="big">
  <iframe src="https://interaktivni.rozhlas.cz/data/vypujcky-2015/charts/nejpujcovanejsi.htm" width="100%" height="800" scrolling="no" frameborder="0"></iframe>
</aside>

Mezi nejčtenější autory už se severské detektivky dostaly, Jo Nesbø je desátý. Seznamu autorů dominuje jiný autor detektivek, český historik Vlastimil Vondruška. Druhou v pořadí je opět autorka detektivních románů Agatha Christie. Na celkové počty výpůjček má ovšem kromě popularity autora vliv také počet jeho knih, který mají pobočky pražské městské knihovny na skladě.

Pro představu, jaká kniha komu udělá radost, jsme se podívali na výpůjčky typické pro jednotlivé věkové kategorie a pohlaví. Tlačítkem vlevo nahoře mezi nimi můžete přepínat. Abychom odfiltrovali málo půjčované exoty, jsou na seznamu pouze knihy s alespoň stovkou výpůjček v dané věkové kategorii.

<aside class="big">
  <iframe src="https://interaktivni.rozhlas.cz/data/vypujcky-2015/charts/top10.htm" width="100%" height="700" scrolling="no" frameborder="0"></iframe>
</aside>

Zejména u žen oblíbené knihy kopírují životní dráhu: po pohádkách a romantických příbězích jsou po 25. roce populární manažerské příručky v čele s publikacemi k cestovnímu ruchu, po 35. roce jsou to manuály k výchově dětí. O dalších deset let později dominuje kniha *Nemoc jako řeč duše* a po 55. roce jsou to opět pohádky.

U mužů jsou trendy poněkud zamlženější, řada knih se na seznamu nejpůjčovanějších knih objevuje v různém věku. Je to mimo jiné tím, že čtenářská pozornost se u méně něžného pohlaví tříští mezi více knih a je tedy jen málo těch, které splní naše kritérium na nejméně stovku výpůjček.

„Máme zkušenost, že muži jsou opravdu při výběru knih vyhraněnější,“ potvrzuje mluvčí pražské městské knihovny Lenka Hanzlíková. „Když už do knihovny dorazí, přicházejí obvykle kvůli svým úzkým koníčkům a přesně vědí, co chtějí. Takže pokud chcete dát knihu jako dárek muži, je potřeba jít na jistotu. U žen obvykle stačí trefit žánr a podívat se, jestli knihu už nemáte v knihovně.“